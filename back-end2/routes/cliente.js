//vou requerer o express como padrao
const express = require('express');

//vou usar o router do express
const router = express.Router();

//vou requerer o controller cliente
const controller = require('../controllers/cliente');


router.post('/', controller.novo);

router.get('/', controller.listar);

router.get('/:id', controller.obterUm);

router.patch('/', controller.atualizar);

router.delete('/:id', controller.deletar);



module.exports = router;