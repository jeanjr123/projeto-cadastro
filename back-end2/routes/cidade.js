const express = require('express');

const router = express.Router();

const controller = require('../controllers/cidade');


router.post('/', controller.novo);

router.get('/', controller.listar);

router.get('/:id', controller.obterUm);

router.patch('/', controller.atualizar);

router.delete('/:id', controller.deletar);



module.exports = router;