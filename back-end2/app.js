var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');


//conexao bando de dados
const db = require('./config/database');
db('mongodb://127.0.0.1:27017/barbapp');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);


const cliente = require('./routes/cliente');
app.use('/cliente', cliente);

const estado = require('./routes/estado');
app.use('/estado', estado);

const cidade = require('./routes/cidade');
app.use('/cidade', cidade);

const telefone = require('./routes/telefone');
app.use('/telefone', telefone);

const salao = require('./routes/salao');
app.use('/salao', salao);

const servico = require('./routes/servico');
app.use('/servico', servico);



const compra = require('./routes/compra');
app.use('/compra', compra);

const cabelereiro = require('./routes/cabelereiro');
app.use('/cabelereiro', cabelereiro);




module.exports = app;