const Estado = require('../models/Estado')(/*construtor*/);

const controller = {};


controller.novo = function(req,res){
   Estado.create(req.body).then(
    //callback do bem
    function(){
        res.send(null);
        res.sendStatus(201).end;
    },
    //callback do mal
    function(erro){
        console.log(erro);
        res.sendStatus(500).end();
    }
   )
}

controller.listar = function(req,res){
   Estado.find().exec().then(
     //callback do bem
     function(estados){   
        res.json(estados).end();           
     },
     //callback do mal
     function(erro){
           console.error(erro);
           res.sendStatus(500).end()
     }
   )
}

controller.obterUm = function(req,res){
   const id = req.params.id;

   Estado.findById(id).exec().then(
    //callback do bem
    function(estado){
        if(estado){
            res.json(estado);
        }
        else{
            res.sendStatus(404).end();
        }
    },
    //callback do mal
    function(erro){
        console.error(erro);
        res.sendStatus(500).end();
        
    }
   );
}

controller.atualizar = function(req,res){
   const id = req.body._id;

   Estado.findOneAndUpdate({_id: id}, req.body).exec().then(
     //callback do bem
     function(estado){
         if(estado){
             res.sendStatus(204).end();
         }
         else{
             res.sendStatus(404).end();
         }
     },
     function(erro){
         console.error(erro);
         res.sendStatus(500).end();
     }
   );
}

controller.deletar = function(req,res){
    const id = req.params.id;

    Estado.findOneAndDelete({_id: id}).exec().then(
     //callback do bem
     function(estado){
         if(estado){
             res.sendStatus(204).end();
         }
         else{
             res.sendStatus(404).end();
         }
         
     },
     //callback do mal
     function(erro){
         console.error(erro);
         res.sendStatus(500).end();      
     }
    )
}


module.exports = controller;