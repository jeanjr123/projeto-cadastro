const Cabelereiro = require('../models/Cabelereiro')(/*construtor*/);

const controller = {};


controller.novo = function(req,res){
  Cabelereiro.create(req.body).then(
    //callback do bem
    function(){
         res.send(null);
         res.sendStatus(201).end();
    },
    //callback do mal
    function(erro){
         console.log(erro);
         res.sendStatus(500).end();
    }
  )
}

controller.listar = function(req,res){
  Cabelereiro.find()
  
  .populate({path:'cidade', populate: {path: 'estado'}})
  
  .exec().then(
    //callback do bem
    function(cabelereiros){ 
        res.json(cabelereiros).end();
    },
    //callback do mal
    function(erro){
        console.error(erro);
        res.sendStatus(500).end();
    }
  )

}

controller.obterUm = function(req,res){
    const id = req.params.id;

    Cabelereiro.findById(id)
    
    .populate({path: 'cidade', populate: {path: 'estado'}})
    
    .exec().then(
        //callback do bem
        function(cabelereiro){
            if(cabelereiro){
                res.json(cabelereiro).end();
            }
            else{
                res.sendStatus(404).end(); //not found
            }
        },
        //callback do mal
        function(erro){
            console.error(erro);
            res.sendStatus(500).end;
        }
    )
}

controller.atualizar = function(req,res){  
  const id = req.body._id;

  Cabelereiro.findOneAndUpdate({_id: id},req.body).exec().then(
     //callback do bem
     function(cabelereiro){
       if(cabelereiro){
           res.sendStatus(204).end();
       }
       else{
           res.sendStatus(404).end();
       }
     },
     //callback do mal
     function(erro){
        console.error(erro);
        res.sendStatus(500).end();
     }
  )
}


controller.delete = function(req,res){
  const id = req.params.id;

  Cabelereiro.findOneAndDelete({_id: id}, req.body).exec().then(
     //callback do bem
     function(cabelereiro){
       if(cabelereiro){
          res.sendStatus(204).end();
       }
       else{
          res.sendStatus(404).end();
       }
     },
     //callback do mal
     function(erro){
          console.error(erro);
          res.sendStatus(500).end();
     }
  )
}






module.exports = controller;