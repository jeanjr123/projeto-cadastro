const Servico = require('../models/Servico')(/*construtor*/);

const controller = {};


controller.novo = function(req,res){
  Servico.create(req.body).then(
    //callback do bem
    function(){
       res.send(null);
       res.sendStatus(201).end();
    },
    //callback do mal
    function(error){
       console.log(error);
       res.sendStatus(500).end();
    }
  )
}

controller.listar = function(req,res){
  Servico.find().exec().then(
     //callback do bem 
     function(servicos){
         res.json(servicos).end();
     },
     //callback do mal
     function(erro){
         console.error(erro);
         res.sendStatus(500).end();
     }
  )
}

controller.obterUm = function(req,res){
   const id = req.params.id;

   Servico.findById(id).exec().then(
     //callback do bem
     function(servico){
         if(servico){ 
             res.json(servico).end();
         }
         else{
             res.sendStatus(404).end();
         }
     },
     //callback do mal
     function(erro){
         console.error(erro);
         res.sendStatus(500).end();
     }
   )
}

controller.atualizar = function(req,res){
    const id = req.body._id;

    Servico.findOneAndUpdate({_id: id}, req.body).exec().then(
        //callback do bem
        function(servico){
            if(servico){
                res.sendStatus(204).end();
            }
            else{
                res.sendStatus(404).end();
            }
        },
        //callback do mal
        function(error){
            console.error(erro);
            res.sendStatus(500).end();
        }
    )
}


controller.deletar = function(req,res){
   const id = req.params.id;

   Servico.findOneAndDelete({_id: id}).exec().then(
       //callback do bem
       function(servico){
           if(servico){
               res.sendStatus(204).end();
           }
           else{
               res.sendStatus(404).end();
           }
       },
       //callback do mal
       function(erro){
           console.error(erro);
           res.sendStatus(500).end();
       }
   );
}


module.exports = controller;