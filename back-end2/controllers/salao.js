const Salao = require('../models/Salao')(/*construtor*/)

const controller = {};


controller.novo = function(req,res){
    Salao.create(req.body).then(
        //callback do bem
        function(){
            res.send(null);
            res.sendStatus(201).end();
        },
        //callback do mal
        function(erro){
            console.log(erro);
            res.sendStatus(500).end();
        }
    )
}


controller.listar = function(req,res){
    Salao.find()

    .populate({path: 'cidade', populate: {path: 'estado'}})
     
    .exec().then(
       //callback do bem 
       function(saloes){
           res.json(saloes).end();
       },
       //callback do mal
       function(erro){
           console.error(erro);
           res.sendStatus(500).end();
       }

    )
}


controller.obterUm = function(req,res){
    const id = req.params.id;
    Salao.findById(id)
    
    .populate({path: 'cidade', populate:{path: 'estado'}})
    
    .exec().then(
       //callback do bem
       function(salao){
           if(salao){
               res.json(salao).end();
           }
           else{
               res.sendStatus(404).end();
           }
       },      
       //callback do mal
       function(erro){
           console.error(erro);
           res.sendStatus(500).end();
       }
    )
}

controller.atualizar = function(req,res){
    const id = req.body._id;

    Salao.findOneAndUpdate({_id: id}, req.body).exec().then(
         //callback do bem
         function(salao){
             if(salao){
                 res.sendStatus(204).end();
             }
             else{
                 res.sendStatus(404).end(); //not found
             }
         },
        //callback do mal
         function(erro){
              console.error(erro);
              res.sendStatus(500).end();     
         }
   )
}


controller.deletar = function(req,res){
     const id = req.params.id;
     Salao.findOneAndDelete({_id: id}).exec().then(
         //callback do bem
         function(salao){
             if(salao){
                 res.sendStatus(204).end();
             }
             else{
                 res.sendStatus(404).end();
             }
         },
         //callback do mal
         function(erro){
             console.error(erro);
             res.sendStatus(500).end();
         }
     )
}

module.exports = controller;

