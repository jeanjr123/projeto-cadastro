const Telefone = require('../models/Telefone')(/*construtor*/);

const controller = {};


controller.novo = function(req,res){
    Telefone.create(req.body).then(
        //callback do bem
        function(){
        res.send(null);
        res.sendStatus(201).end();
        },
        //callback do mal
        function(error){
            res.send(error);
            res.sendStatus(500).end();
        }

    )

}

controller.listar = function(req,res){
     Telefone.find()

     .populate({path: 'cliente', populate:{path: 'cidade', populate:{path: 'estado'}}})
     
     .exec().then(
      //callback do bem
      function(telefones){    
         res.json(telefones).end();  
      },
      function(erro){
         console.error(erro);
         res.sendStatus(500).end();
      }

     )
}

controller.obterUm = function(req,res){
   const id = req.params.id;

   Telefone.findById(id)
   
   .populate({path: 'cliente', populate:{path: 'cidade', populate:{path: 'estado'}}})
   
   
   .exec().then(
    //callback do bem
    function(telefone){
        if(telefone){
            res.json(telefone);
        }
        else{
            res.sendStatus(404).end(); //not found
        }
    },
    //callback do mal
    function(erro){
        console.error(erro);
        res.sendStatus(500).end();
    }
   )
}


controller.atualizar = function(req,res){
    const id = req.body._id;

    Telefone.findOneAndUpdate({_id: id}, req.body)
    
    .populate({path: 'cliente', populate:{path: 'cidade', populate:{path: 'estado'}}})
   
    
    .exec().then(
     //callback do bem
     function(telefone){
        if(telefone){
            res.sendStatus(204).end();
        }
        else{
            res.sendStatus(404).end();
        }    
     },
     //callback do mal
     function(erro){
        console.error(erro);
        res.sendStatus(500).end;
     }
    )
}

controller.deletar = function(req,res){
   const id = req.params.id;
   Telefone.findOneAndDelete({_id: id}).exec().then(
    //callback do bem
      function(telefone){
          if(telefone){
              res.sendStatus(204).end();
          }
          else{
              res.sendStatus(404).end();
          }
      },
      //callback do mal
      function(erro){
          console.error(erro);
          res.sendStatus(500).end();
      }
   )
}



module.exports = controller;


