const Compra = require('../models/Compra')(/*construtor*/);

const controller = {};


controller.novo = function(req,res){
   Compra.create(req.body).then(
       //callback do bem
       function(){
           res.send(null);
           res.sendStatus(201).end();
       },
       //callback do mal
       function(erro){
           console.log(erro);
           res.sendStatus(500).end();
       }
   )
}



controller.listar = function(req,res){
  
   Compra.find().populate('servico') 
   
   .populate({path:'cliente', populate:{path: 'cidade', populate:{path: 'estado'}}})
   
   .populate({path:'cabelereiro', populate:{path: 'cidade', populate:{path: 'estado'}}})
   
   .then(
     //callback do bem
     function(compras){
         res.json(compras).end();
     },
     //callback do mal
     function(erro){
         console.log(erro);
         res.sendStatus(500).end();
     }
   )
}

controller.obterUm = function(req,res){
    const id = req.params.id;

    Compra.findById(id).populate('servico')
    
    .populate({path:'cliente', populate:{path: 'cidade', populate:{path: 'estado'}}})

    .populate({path:'cabelereiro', populate:{path: 'cidade', populate:{path: 'estado'}}})
    
    .exec().then(
     //callback do bem
     function(compra){
        if(compra){
            res.json(compra).end();
        }
        else{
            res.sendStatus(404).end(); //not found
        }
     },
     //callback do mal
     function(erro){
         console.error(erro);
         res.sendStatus(500).end();
     }
    )
}

controller.atualizar = function(req,res){
   const id = req.body._id;

   Compra.findOneAndUpdate({_id: id}, req.body).exec().then(
       //callback do bem
       function(compra){
           if(compra){
               res.sendStatus(204).end();
           }
           else{
               res.sendStatus(404).end();
           }
       },
       //callback do mal
       function(erro){
           console.error(erro);
           res.sendStatus(500).end();
       }
   )
}

controller.deletar = function(req,res){
    const id = req.params.id;

    Compra.findOneAndDelete({_id: id}).exec().then(
        //callback do mal
        function(compra){
            if(compra){
                res.sendStatus(204).end();
            }
            else{
                res.sendStatus(404).end();
            }
        },
        //callback do mal
        function(erro){
           console.error(erro);
           res.sendStatus(404).end();
        }
    )
}





module.exports = controller;