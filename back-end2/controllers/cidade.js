const Cidade = require('../models/Cidade')(/*construtor*/);

const controller = {};


controller.novo = function(req,res){
    Cidade.create(req.body).then(
       function(){
           res.send(null);
           res.sendStatus(201).end();
       },
       function(erro){
           console.log(error);
           res.sendStatus(500).end();
       }
    )

}

controller.listar = function(req,res){
    Cidade.find().populate('estado').exec().then(
       function(cidades){
           res.json(cidades).end();
       },
       function(erro){
           console.error(erro);
           res.sendStatus(500).end();
       }
    );
}

controller.obterUm = function(req,res){
    const id = req.params.id;

    Cidade.findById(id).populate('estado').exec().then(
        function(cidades){
            if(cidades){
                res.json(cidades).end();
            }
            else{
                res.sendStatus(404).end();
            }
        },
        function(erro){
            console.log(erro);
            res.sendStatus(500).end();
        }

    )
}

controller.atualizar = function(req,res){
    const id = req.body._id;

    Cidade.findOneAndUpdate({_id: id}, req.body).exec().then(
        function(cidade){
            if(cidade){
                res.sendStatus(204).end();
            }
            else{
                res.sendStatus(404).end(); //not found
            }
        },
        function(erro){
            console.error(erro);
            res.sendStatus(500).end(); //internal server error
        }
    )
}


controller.deletar = function(req,res){
    const id = req.params.id;

    Cidade.findOneAndDelete({_id: id}).exec().then(
        function(cidade){
            if(cidade){
                res.sendStatus(204).end();
            }
            else{
                res.sendStatus(404).end();
            }
        },
        function(erro){
            console.error(erro);
            res.sendStatus(500).end();
        }

    );

}





module.exports = controller;