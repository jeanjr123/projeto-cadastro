//o arquivo controler do veiculo tem quer requerer seu model
const Cliente = require('../models/Cliente')(/*construtor*/);

//criando o controler

const controller = {}; //objeto vazio

//toda funcao de controler que estiver ligada a model deve
// ter dois parametros o de requisicao e o de resposta

controller.novo = function(req,res){
    Cliente.create(req.body).then(
        //Callback se der certo
         function(){
             res.send(null);//resposta sem conteudo
             res.sendStatus(201).end();
         },
         //callback se der errado
         function(erro){
             console.log(erro);//mostra erro no terminal
             //HTTP 500: Erro interno no servidor
             res.sendStatus(500).end();
         }
   );

}

controller.listar = function(req,res){
     Cliente.find()
     
     .populate({path: 'cidade', populate: {path: 'estado'}})
     
     .exec().then(
        //Callback do bem
        function(clientes){
            res.json(clientes).end()
        },

        //Callback do mal
        function(erro){
            console.error(erro);
            res.sendStatus(500).end();
        }
         
     );

}

controller.obterUm = function(req,res){
    //vou fazer uma requisicao passando como parametro o id
    const id = req.params.id;

    //busca por id
    Cliente.findById(id).exec().then(
     //Callback do bem
     function(cliente){
         if(cliente){  //cliente encontrado
             res.json(cliente).end();
         }
         else{//cliente nao encontrado
            //HTTP: 404 nao encontrado
             res.sendStatus(404).end();
         }
     },
     //Callback do mal
     function(erro){
         console.error(erro).end();
         res.sendStatus(500).end();
     }
    );
}


controller.atualizar = function(req,res){
   //faz a requisicao pelo campo id 
   const id = req.body._id;

   //pega pelo id, e substitui pelo req.body
   Cliente.findOneAndUpdate({_id: id},req.body).exec().then(
       //Callback do bem
       function(cliente){
           if(cliente){//encontrou o cadastro e atualizou
              //HTTP: 204: ok
              res.sendStatus(204).end();               
           }
           else{//nao encontrou e nao atualizou
              res.sendStatus(404).end();
           }
       },

       //Callback do mal
       function(erro){
           console.error(erro).end();
           res.sendStatus(500).end();
       }
   );

}

controller.deletar = function(req,res){
 const id = req.params.id;

 Cliente.findOneAndDelete({_id: id}).exec().then(
    //Callback do bem
    function(cliente){ //encontrou e deletou
      if(cliente){
          res.sendStatus(204).end();
      }   
      else{//nao encontrou e nao deletou
          res.sendStatus(404).end();
      }      
    },
    //Callback do mal
    function(error){
        console.Error(erro).end();
        res.sendStatus(500).end();
    }

 );

}
//exportando o controller

module.exports = controller;




