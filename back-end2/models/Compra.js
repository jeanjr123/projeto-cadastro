const mongoose = require('mongoose');

module.exports = function(){
    const schema = mongoose.Schema({

        cliente:{
            type: mongoose.ObjectId,
            ref:"Cliente",
            required: true
        },

        servico:{
            type: mongoose.ObjectId,
            ref: "Servico",
            required: true
        },

        cabelereiro:{
            type: mongoose.ObjectId,
            ref: "Cabelereiro",
            required: true
        },
     
         data:{
             type: Date,
             required: true
         },

         hora:{
             type:String,
             required: true
         }
    })

    return mongoose.model('Compra', schema , 'compras');
}

