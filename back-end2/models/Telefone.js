const mongoose = require('mongoose');


module.exports = function(){
   const schema = mongoose.Schema({
    
        cliente:{
             type: mongoose.ObjectId,
             ref:"Cliente"
        },

        telefone:{
            type: String,
            required: true,
        }

   });

   return mongoose.model('Telefone', schema , 'telefones');

}



