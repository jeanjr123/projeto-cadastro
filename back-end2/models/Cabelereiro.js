const mongoose = require('mongoose');

module.exports = function(){
    const schema = mongoose.Schema({
        nome:{
            type: String,
            required: true
        },
        cpf:{
            type: Number,
            required: true,
    
            index:{
                unique: true
            },
        },
        rg:{
            type: Number,
            required: true, 
            index:{
                unique: true
            },
        },    

        endereco_logradouro:{
            type: String,
            required: true
        },
        numero:{
            type: Number,
            required: true
        },

        bairro:{
            type:String,
            requiered: true
        },

        cidade:{
            type: mongoose.ObjectId,
            ref: "Cidade",
            required: true
        },

        cep:{
            type: String,
            required: true
        }, 
        
        telefone_1:{
            type: String,
            required: true
        },

        telefone_2:{
            type: String,
            required: false
        },
        
        dataNasc:{
            
            type: String,
            required: true
        },

        email:{
            type: String,
            required:true
        }, 
    
    });
   
    return mongoose.model('Cabelereiro', schema, 'cabelereiros');
}