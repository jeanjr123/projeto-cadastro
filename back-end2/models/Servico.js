const mongoose = require('mongoose');

module.exports = function(req,res){
     const schema = mongoose.Schema({

      descricao:{
          type: String,
          required: true
      },

      tempoEstimado:{
          type: Number,
          required: true
      },

      preco:{
          type: Number,
          required: true
      }

     })
     
     return mongoose.model('Servico', schema, 'servicos');

}