//Cliente com letra maiuscula porque e um model

const mongoose = require('mongoose');

module.exports = function(){
    const schema = mongoose.Schema({
        nome:{
            type: String,
            required: true
        },
        cpf:{
            type: Number ,
            required: true,
            //unique key
            index:{
                unique: true
            },
        },
        rg:{
            type: Number,
            required: true,  ///colocar opcional
            index:{
                unique: true
            },
        },    

        endereco_logradouro:{
            type: String,
            required: true
        },
        numero:{
            type:Number,
            required: true
        },

        bairro:{
            type:String,
            required: true
        },

        cidade:{
            type: mongoose.ObjectId,
            ref: "Cidade",
            required: true
        },

        cep:{
            type: String,
            required: true
        },       
        
        dataNasc:{
            type: String,
            required: true
        },

        email:{
            type: String,
            required:true
        }, 
        
        nroCartao:{
            type: Number
        }

    
    });
    // o meu model Cliente vai retornar a colecao clientes
    return mongoose.model('Cliente', schema, 'clientes');
}