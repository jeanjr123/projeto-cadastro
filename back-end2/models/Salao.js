const mongoose = require('mongoose');

module.exports = function(){
   const schema = mongoose.Schema({
        
         razaoSocial:{
             type:String,
             required:true,

             index:{
                unique: true
            }
         },

         nomeFantasia:{
             type:String,
             required: true,

             index:{
                unique: true
            }
         },

         cnpj:{
             type:String,
             required:true,

             index:{
                 unique: true
             }
         },

         inscricaoEstadual:{
             type:String,
             required:true,

             index:{
                unique: true
            }

         },

         telefone1:{
             type:String,
             required:true
         },

         telefone2:{
             type:String
         },

         endereco_logradouro:{
             type:String,
             required:true
         },
         
         numero:{
             type: Number,
             required: true
         },

         bairro:{
             type:String,
             required:true
         },

         cidade:{
             type: mongoose.ObjectId,
             ref:"Cidade",
             required: true
         },

         cep:{
             type:String,
             required: true
         },
 
         email:{
             type:String,
             required: true
         }

   })
  
   return mongoose.model('Salao', schema , 'saloes');
}

