const mongoose = require('mongoose');

module.exports = function(){
   const schema = mongoose.Schema({
     
       nome:{
           type:String,
           required: true
       },

       estado:{
           type: mongoose.ObjectId,
           ref:"Estado",
           required: true
       }
   })

   return mongoose.model('Cidade', schema, 'cidades');

}