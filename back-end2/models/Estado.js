const mongoose = require('mongoose');


module.exports = function(){
     const schema = mongoose.Schema({
   
        sigla:{
            type: String,
            required:true,

            index:{
              unique: true
            }

  
        }   

     });

       return mongoose.model('Estado', schema, 'estados');

}

